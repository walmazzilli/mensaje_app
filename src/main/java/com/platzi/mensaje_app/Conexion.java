package com.platzi.mensaje_app;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class Conexion {

    String url = "jdbc:mysql://localhost:8081/mensajes_app";
    String user = "root";
    String password = "";
    Connection conn = null;

    public Connection get_connection() {
        try {
            conn = DriverManager.getConnection(url, user, password);
            if(conn != null){
                System.out.println("Conexión exitosa");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
}
